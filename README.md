# Trakken Coding Challenge

## Introduction
The task description is designed to take longer than an hour to complete. You do not have to finish all tasks. Please feel free to stop after an hour and just hand in what you have finished until then. **We do not consider it a disadvantage if you could not complete all the tasks in that hour.** Feel free to add comments if you get stuck or run out of time before you finish.
   
The purpose of this task is to test your familiarity with Python and its basic ecosystem. The most important aspect of your solution is that it provides the correct solution. However, we will also take coding style into consideration. If you have any questions, feel free to reach out to us at any time.

**Hint**: Consider using pipenv to quickly setup your local environment

## Problem description

We ask you to write a small utility that reads out a csv file containing a list of years, and produces a number of JSON files containing the start and end dates of each month within that year.

The input file will look similar to this:

```csv
year
1993
1996
2000
2222
```

An output JSON file is expected to look like this:
```json
{
    "1": {"first": "01-01-1993", "last": "31-01-1993"},
    ...
    "12": {"first": "01-12-1993", "last": "31-12-1993"},
}
```

## Tasks
Your job is to extend the `main.py` so that the test stage in the CI/CD finishes successfully. This will requires your code to:

1. Read all values from the `years.csv` file
2. Compute the first and last days within a month for the given year
3. Write all results into a JSON file within a given directory. Each file should obey the naming convention `[YEAR].json`

You can use any standard library that comes with 3.7.

**BONUS:** 
Format your code, so that the lint stage of the GitLab CI/CD pipeline does not fail.

**BONUS:**  
Add a new test file and write additional unit tests that check if your solution works correctly for:
1. leap years
2. negative years (should raise a `ValueError`)
3. empty values (should not create any file)

