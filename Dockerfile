FROM python:3.7-slim

WORKDIR /usr/app
COPY Pipfile.lock Pipfile.lock
COPY Pipfile Pipfile

RUN apt-get update && \
    pip install --no-cache-dir pipenv && \
    pipenv install --system --ignore-pipfile --dev
